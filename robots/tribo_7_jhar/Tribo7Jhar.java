package tribo_7_jhar;
import robocode.*;
import java.awt.Color;
 
public class Tribo7Jhar extends AdvancedRobot
{
    static double movimento;
    static double distancia;
    static String inimigo;
 
    private Color Cor(int r, int g, int b) {
        return new Color(r, g, b);
    }
 
    public void run() {
        setBodyColor(Cor(40,40,40));
        setGunColor(Cor(175,12,21));
        setRadarColor(Cor(13,53,128));
        setScanColor(Color.red);
        setBulletColor(Color.orange);
       
        movimento = Double.MAX_VALUE;
 
        setAdjustGunForRobotTurn(true);
        onRobotDeath(null);
 
        turnRadarRightRadians(Double.MAX_VALUE);
 
        execute();
    }
 
    public void onScannedRobot(ScannedRobotEvent e) {
 
        double inimigoDistancia = e.getDistance();
 
        if(getDistanceRemaining() == 0){
 
            setAhead(movimento = -movimento);
            setTurnRightRadians(90);
        }
 
        if(distancia > inimigoDistancia){
 
            distancia = inimigoDistancia;
            inimigo = e.getName();
 
            out.println("Você está encrencado agora, " + inimigo);
        }
 
        if(inimigo.equals(e.getName())){
 
            if(getGunHeat() < 1 && inimigoDistancia < 600){
 
                if(getGunHeat() == getGunTurnRemaining()) {
                    setFireBullet(getEnergy() * 15 / inimigoDistancia);
                    onRobotDeath(null);
                }
 
                setTurnRadarLeft(getRadarTurnRemaining());
            }
 
            inimigoDistancia = e.getBearingRadians() + getHeadingRadians();
            setTurnGunRightRadians(robocode.util.Utils.normalRelativeAngle(inimigoDistancia - getGunHeadingRadians() + (1 - e.getDistance() / 600) * Math.asin(e.getVelocity() / 11) * Math.sin(e.getHeadingRadians() - inimigoDistancia)));
        }
 
        execute();
    }
 
    public void onHitWall(HitWallEvent e) {
 
        out.println("Ai! Bati na parede!");
        //back(100);
        //setTurnRight(90);
        if(Math.abs(movimento) > 200){
            movimento = 200;
        }
        execute();
    }
 
    public void onRobotDeath(RobotDeathEvent e){
 
        distancia = Double.MAX_VALUE;
        execute();
    }
}
